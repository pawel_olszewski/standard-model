var getMass = function (mass) {
	if (mass == "0" || mass === "~0") {
		return mass;
	} else {
		return mass.slice(0, mass.length - 1);
	}
}

var getUnit = function (mass) {
	if (mass[mass.length - 1] === "g")  {
		return <React.Fragment>GeV/c<sup>2</sup></React.Fragment>;
	} else if (mass[mass.length - 1] === "m")  {
		return <span>MeV/c<sup>2</sup></span>;
	} else {
		return;
	}
}






class Particle extends React.Component {


	constructor(props) {
		super(props);


		this.state = {
			show: true

		};

		this.eventHandler = this.eventHandler.bind(this);

	}




	eventHandler() {
		this.setState({
			show: !this.state.show
		});
	}



	render () {

		var style = {

			height: "100%",
			width: "100%",
			gridArea: this.props.place,

		};


		if (this.state.show) {



			return(
				<div style={style} onClick={this.eventHandler}>
				<PageOne  {...this.props} />


				</div>
				)

			} else {

				return(<div  style={style} onClick={this.eventHandler}>
					<PageTwo {...this.props} />
				</div>)

			}

		}
	}




	class PageOne extends React.Component {
		render () {

			return(
			<React.Fragment>
				<Symbol {...this.props} />
				<Description {...this.props} />
			</React.Fragment>
			)

		}
	}




	class Symbol extends React.Component {
		render () {

			var style = {
				height: "80%",
				width: "100%",
				backgroundColor: this.props.color,
				fontSize: 110,
				textAlign: "center",
			};



			return(
			<div style={style}>{this.props.symbol}</div>
			)

		}
	}

	class Description extends React.Component {
		render () {

			var style = {
				height: "20%",
				width: "100%",
				backgroundColor: this.props.color,
				fontSize: 18,
				textAlign: "center",
			};

			return(
			<div style={style}>{this.props.description}</div>
			)

		}
	}

	class PageTwo extends React.Component {
		render () {

			var style = {
				height: "100%",
				width: "100%",
				backgroundColor: this.props.color,
				fontSize: 16,
				textAlign: "center",
			};

			var tableStyle ={
				height: 180,
				width: 130,

			}

			return(
		<div style={style}>



			<table style={tableStyle}>
				<tbody>

					<tr>
						<th colSpan="2">
							Masa
						</th>
					</tr>
					<tr>
						<td colSpan="2"> 
							{getMass(this.props.mass)}  {getUnit(this.props.mass)}
						</td>
					</tr>
					<tr>
						<th>
							Ładunek <br /> elektryczny
						</th>
						<th> Spin
						</th>
					</tr>

					<tr>
						<td>
							{this.props.q}
						</td>
						<td> {this.props.spin}
						</td>
					</tr>
				</tbody>

			</table>

		</div>
			)

		}
	}







	var page = document.getElementById("page");

	var e = <span>e<sup>-</sup></span>;
	var m = <span>μ<sup>-</sup></span>;
	var tau = <span>τ<sup>-</sup></span>;

	var ne = <span>ν<sub>e</sub></span>;

	var nm = <span>ν<sub>μ</sub></span>;

	var nt = <span>ν<sub>τ</sub></span>;

	var W = <span>W<sup className="small">±</sup></span>;

	ReactDOM.render(

	<React.Fragment>



    	<Particle color="purple" place="u" symbol="u" description="Kwark górny" q="+2/3" spin="1/2" mass="1-4m"/>
    	<Particle color="purple" place="c" symbol="c" description="Kwark powabny"  q="+2/3" spin="1/2" mass="0.08-0.013g" />
    	<Particle color="purple" place="t" symbol="t" description="Kwark szczytowy"  q="+2/3" spin="1/2" mass="173g" />
    	<Particle color="red" place="gamma" symbol="𝛾" description="Foton"  q="0" spin="1" mass="0" />


    	<Particle color="gold" place="higgs" symbol="H" description="Bozon Higgsa"  q="0" spin="0" mass="126g" />




    	<Particle color="purple" place="d" symbol="d" description="Kwark dolny"  q="-1/3" spin="1/2"  mass="4-8m"/>
    	<Particle color="purple" place="s" symbol="s" description="Kwark dziwny" q="-1/3" spin="1/2"  mass="1.15-1.35g"/>
    	<Particle color="purple" place="b" symbol="b" description="Kwark spodni" q="-1/3" spin="1/2"  mass="4.1-4.4g"/>
    	<Particle color="red" place="gluon" symbol="g" description="Gluon"  q="0" spin="1"  mass="0"/>



    	<Particle color="green" place="e" symbol={e} description="Elektron" q="-1" spin="1/2" mass="0.510999m" />
    	<Particle color="green" place="m" symbol={m} description="Mion" q="-1" spin="1/2" mass="105.6584m" />
    	<Particle color="green" place="tau" symbol={tau} description="Taon" q="-1"  spin="1/2" mass="1.7768g"/>
    	<Particle color="red" place="Z" symbol="Z" description="Bozon Z"  q="0"  spin="1" mass="91.188g"/>



    	<Particle color="green" place="ne" symbol={ne} description="Neutrino elektronowe" q="0" spin="1/2"  mass="~0"/>
    	<Particle color="green" place="nm" symbol={nm} description="Neutrino mionowe" q="0" spin="1/2" mass="~0" />
    	<Particle color="green" place="nt" symbol={nt} description="Neutrino taonowe"  q="0" spin="1/2" mass="~0" />
    	<Particle color="red" place="W" symbol={W} description="Bozon W" q="±1" spin="1" mass="80.399g" />

	</React.Fragment>,


	page);