Strona w React.js prezentująca cząstki fundamentalne modelu standardowego. Po kliknięciu na pole cząstki pojawią się jej masa, ładunek i spin.

Celowo zamieszczam kod JSX zamiast przekonwertowanego JavaScript - chcę pokazać umiejętość korzystania z React.js.